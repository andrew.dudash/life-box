import logging
import threading
import socket
import json
import math
import argparse

import pygame
from pygame.locals import *

import network
import game
import message
import render


WORLD_LOCK = threading.Lock()
WORLD = game.World()
WORLD_READY_EVENT = threading.Event()
WORLD_PERIOD = 0.01
SERVER_MESSAGE_THREAD = None
SERVER_MESSAGE_THREAD_KILL_EVENT = threading.Event()
MAP_LOCK = threading.Lock()
MAP = game.NULL_MAP


def server_message_thread_callback(client_socket, kill_event):
    while not kill_event.is_set():
        a_message_raw, a_server = CLIENT_SOCKET.recvfrom(network.MAX_MESSAGE_LENGTH)
        a_message = json.loads(str(a_message_raw, 'ascii'))
        message_type = a_message['message-type']
        if message_type == 'world-status':
            WORLD_READY_EVENT.set()
            with WORLD_LOCK:
                WORLD.set_players([
                    game.Player(
                        identity=player['identity'],
                        position=game.Position(
                            player['position']['x'],
                            player['position']['y']
                        ),
                        direction = player['direction']
                    )
                    for player in a_message['players']
                ])
        elif message_type == 'map':
            with MAP_LOCK:
                global MAP
                MAP = game.Map(
                    width=a_message['grid']['width'],
                    height=a_message['grid']['width'],
                    palette=[
                        game.TileType(tile_type['type'])
                        for tile_type in a_message['palette']
                    ],
                    tiles=a_message['grid']['tiles']
                )
                logging.info('New map loaded.')
        else:
            logging.warn(
                'Unrecognized message: %s',
                a_message
            )
        kill_event.wait(WORLD_PERIOD)


def register(client_socket, target):
    logging.info('Client Registering')
    client_socket.sendto(
        message.make_registration_request_message(),
        target
    )
    a_message, a_server = client_socket.recvfrom(
        network.MAX_MESSAGE_LENGTH
    )
    identity = json.loads(str(a_message, 'ascii'))['identity']
    with WORLD_LOCK:
        WORLD.add_player(
            game.Player(identity, game.Position(0, 0), 0)
        )
    logging.info('Client Registration Success: {}'.format(identity))
    return identity


KEY_TO_ACTION_MAP = {
    K_UP: game.Action('move-forward'),
    K_DOWN: game.Action('move-backward'),
    K_RIGHT: game.Action('strafe-right'),
    K_LEFT: game.Action('strafe-left')
}

    
if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    ARGUMENT_PARSER = argparse.ArgumentParser()
    ARGUMENT_PARSER.add_argument(
        '--render-mode',
        help='Pick the rendering mode.',
        type=str,
        choices=['TOP-DOWN', 'FIRST-PERSON'],
        default='FIRST-PERSON'
    )
    ARGUMENT_PARSER.add_argument(
        '--correct-fish-eye',
        help='Remove fish eye effect from renderer.',
        action=argparse.BooleanOptionalAction,
        default=True
    )
    ARGS = ARGUMENT_PARSER.parse_args()
    
    TARGET = ('localhost', network.DEFAULT_SERVER_PORT)
    CLIENT_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    IDENTITY = register(CLIENT_SOCKET, TARGET)

    WIDTH, HEIGHT = 800, 600
    pygame.init()
    SCREEN = pygame.display.set_mode((WIDTH, HEIGHT,))
    pygame.display.set_caption('Life Box')

    BACKGROUND = pygame.Surface(SCREEN.get_size())
    BACKGROUND = BACKGROUND.convert()
    BACKGROUND.fill((255, 255, 255))

    PLAYER_IMAGE = pygame.Surface((32, 32))
    PLAYER_IMAGE = PLAYER_IMAGE.convert()
    PLAYER_IMAGE.fill((128, 128, 128))

    EMPTY_TILE_IMAGE = pygame.Surface((game.TILE_WIDTH, game.TILE_HEIGHT))
    EMPTY_TILE_IMAGE = EMPTY_TILE_IMAGE.convert()
    EMPTY_TILE_IMAGE.fill((172, 172, 172))

    OPAQUE_TILE_IMAGE = pygame.Surface((game.TILE_WIDTH, game.TILE_HEIGHT))
    OPAQUE_TILE_IMAGE = OPAQUE_TILE_IMAGE.convert()
    OPAQUE_TILE_IMAGE.fill((64, 0, 0))

    TILE_MAPPING = {
        'opaque': OPAQUE_TILE_IMAGE,
        'empty': EMPTY_TILE_IMAGE
    }

    SCREEN.blit(BACKGROUND, (0, 0))
    pygame.display.flip()

    CLOCK = pygame.time.Clock()

    SERVER_MESSAGE_THREAD = threading.Thread(
        target=server_message_thread_callback,
        args=(CLIENT_SOCKET, SERVER_MESSAGE_THREAD_KILL_EVENT)
    )
    SERVER_MESSAGE_THREAD.start()

    TOP_DOWN_RENDERER = render.make_top_down_renderer(
        player_image=PLAYER_IMAGE,
        depth_of_field=20,
        field_of_view=math.pi / 2,
        ray_count=200,
        tile_mapping=TILE_MAPPING,
        correct_fish_eye=ARGS.correct_fish_eye
    )

    FIRST_PERSON_RENDERER = render.make_first_person_renderer(
        depth_of_field=20,
        field_of_view=math.pi / 2,
        ray_count=400,
        tile_mapping=TILE_MAPPING,
        correct_fish_eye=ARGS.correct_fish_eye
    )

    pygame.mouse.set_visible(False)
    pygame.event.set_grab(True)
    MOUSE_FREE = False
    RUNNING = True
    while RUNNING:
        ACTIONS = []
        for EVENT in pygame.event.get():
            if EVENT.type == pygame.QUIT:
                RUNNING = False
            elif EVENT.type == MOUSEMOTION:
                if EVENT.rel[0] > 0:
                    ACTIONS.append(game.Action(
                        'turn-right',
                        EVENT.rel[0]
                    ))
                elif EVENT.rel[0] < 0:
                    ACTIONS.append(game.Action(
                        'turn-left',
                        abs(EVENT.rel[0])
                    ))                    
            else:
                logging.debug(
                    'Unknown event: %s',
                    EVENT
                )
        KEYS_PRESSED = pygame.key.get_pressed()
        if KEYS_PRESSED[K_ESCAPE]:
            if MOUSE_FREE:
                MOUSE_FREE = False
                pygame.mouse.set_visible(True)
                pygame.event.set_grab(False)
            else:
                MOUSE_FREE = True
                pygame.mouse.set_visible(False)
                pygame.event.set_grab(True)
        for KEY, ACTION in KEY_TO_ACTION_MAP.items():
            if KEYS_PRESSED[KEY]:
                ACTIONS.append(ACTION)
                
        CLIENT_SOCKET.sendto(
            message.make_client_actions_message(
                IDENTITY,
                ACTIONS
            ),
            TARGET
        )
        SCREEN.blit(BACKGROUND, (0, 0))
        with WORLD_LOCK, MAP_LOCK:
            # Client Side Prediction
            for ACTION in ACTIONS:
                WORLD.perform_action(IDENTITY, ACTION, MAP)
        with WORLD_LOCK, MAP_LOCK:
            PLAYER = WORLD.get_player(IDENTITY)
            if ARGS.render_mode == 'TOP-DOWN':
                TOP_DOWN_RENDERER(
                    world=WORLD,
                    map=MAP,
                    surface=SCREEN,
                )
            elif ARGS.render_mode == 'FIRST-PERSON':
                FIRST_PERSON_RENDERER(
                    player=PLAYER,
                    world=WORLD,
                    map=MAP,
                    surface=SCREEN
                )
            else:
                logging.warn('Unknown render mode: {}'.format(ARGS.render_mode))

        pygame.display.flip()
        CLOCK.tick(game.DEFAULT_TICK_FREQUENCY)

    SERVER_MESSAGE_THREAD_KILL_EVENT.set()
    pygame.quit()
    SERVER_MESSAGE_THREAD.join()
