import uuid
import math

TILE_WIDTH = 32
TILE_HEIGHT = 32
FIELD_OF_VIEW = 90
DEFAULT_TICK_FREQUENCY = 66.6666 # The amount of frames per second.

class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Position({}, {})'.format(self.x, self.y)

class TileType:
    def __init__(self, type):
        self.type = type


class Map:
    def __init__(self, width, height, palette, tiles, spawn=Position(0, 0)):
        if not len(tiles) == (width * height):
            raise ValueError('There are only {} tiles in this map, but the dimensions are {} by {}'.format(len(tiles), width, height))
        self.width = width
        self.height = height
        self.palette = palette
        self.tiles = tiles

    def is_occupied(self, position):
        try:
            return self.palette[self.tiles[position.y * self.width + position.x]].type != 'empty'
        except IndexError:
            #Warning! Tracing outside of map. Leak detected.
            return False

    def __iter__(self):
        for x in range(self.width):
            for y in range(self.height):
                yield (Position(x, y), self.palette[self.tiles[y * self.width + x]])

NULL_MAP = Map(
    width=0,
    height=0,
    palette=[],
    tiles=[]
)


class Player:
    def __init__(self, identity, position, direction):
        self.identity = identity
        self.position = position
        self.direction = direction
        self.move_direction_vector = Position(0, 0)
        self.strafe_direction_vector = Position(0, 0)
        self.__update_direction_vectors()
        self.move_speed = 2
        self.strafe_speed = 2
        self.turn_speed = 0.01

    def get_position(self):
        return Position(
            int(self.position.x),
            int(self.position.y)
        )

    def __normalize_direction(self):
        while self.direction >= (math.pi * 2):
            self.direction -= math.pi * 2
        while self.direction < 0:
            self.direction += math.pi * 2

    def __update_direction_vectors(self):
        self.move_direction_vector.x = math.cos(self.direction)
        self.move_direction_vector.y = -math.sin(self.direction)
        self.strafe_direction_vector.x = math.cos(self.direction + (math.pi / 2))
        self.strafe_direction_vector.y = -math.sin(self.direction + (math.pi / 2))

    def move_forward(self, map):
        new_x = self.position.x + self.move_direction_vector.x * self.move_speed
        new_y = self.position.y + self.move_direction_vector.y * self.move_speed
        if not map.is_occupied(Position(int(self.position.x / TILE_WIDTH), int(new_y / TILE_HEIGHT))):
            self.position.y = new_y
        if not map.is_occupied(Position(int(new_x / TILE_WIDTH), int(self.position.y / TILE_HEIGHT))):
            self.position.x = new_x

    def move_backward(self, map):
        new_x = self.position.x - self.move_direction_vector.x * self.move_speed
        new_y = self.position.y - self.move_direction_vector.y * self.move_speed
        if not map.is_occupied(Position(int(self.position.x / TILE_WIDTH), int(new_y / TILE_HEIGHT))):
            self.position.y = new_y
        if not map.is_occupied(Position(int(new_x / TILE_WIDTH), int(self.position.y / TILE_HEIGHT))):
            self.position.x = new_x

    def turn_right(self, delta, map):
        self.direction -= self.turn_speed * delta
        self.__normalize_direction()
        self.__update_direction_vectors()

    def turn_left(self, delta, map):
        self.direction += self.turn_speed * delta
        self.__normalize_direction()
        self.__update_direction_vectors()

    def strafe_left(self, map):
        new_x = self.position.x + self.strafe_direction_vector.x * self.strafe_speed
        new_y = self.position.y + self.strafe_direction_vector.y * self.strafe_speed
        if not map.is_occupied(Position(int(self.position.x / TILE_WIDTH), int(new_y / TILE_HEIGHT))):
            self.position.y = new_y
        if not map.is_occupied(Position(int(new_x / TILE_WIDTH), int(self.position.y / TILE_HEIGHT))):
            self.position.x = new_x


    def strafe_right(self, map):
        new_x = self.position.x - self.strafe_direction_vector.x * self.strafe_speed
        new_y = self.position.y - self.strafe_direction_vector.y * self.strafe_speed
        if not map.is_occupied(Position(int(self.position.x / TILE_WIDTH), int(new_y / TILE_HEIGHT))):
            self.position.y = new_y
        if not map.is_occupied(Position(int(new_x / TILE_WIDTH), int(self.position.y / TILE_HEIGHT))):
            self.position.x = new_x


class World:
    def __init__(self):
        self.players = []
        self.player_cache = {}

    def update(self):
        pass

    def generate_spawn_position(self):
        return Position(200, 200)

    def set_map(self, map):
        pass

    def generate_player(self):
        identity = str(uuid.uuid4())
        new_player = Player(
            identity=identity,
            position=self.generate_spawn_position(),
            direction=0
        )
        self.add_player(new_player)
        return identity

    def add_player(self, new_player):
        self.players.append(new_player)
        self.player_cache[new_player.identity] = new_player

    def set_players(self, new_players):
        self.players = new_players
        self.player_cache.clear()
        for player in self.players:
            self.player_cache[player.identity] = player

    def remove_player(self, old_player_identity):
        old_player = self.player_cache[old_player_identity]
        self.players.remove(old_player)
        del self.player_cache[old_player.identity]

    def get_player(self, identity):
        return self.player_cache[identity]

    def perform_action(self, identity, action, map):
        action_type = action.action_type
        player = self.get_player(identity)
        if action_type == 'turn-left':
            player.turn_left(action.delta, map)
        elif action_type == 'turn-right':
            player.turn_right(action.delta, map)
        elif action_type == 'strafe-left':
            player.strafe_left(map)
        elif action_type == 'strafe-right':
            player.strafe_right(map)
        elif action_type == 'move-forward':
            player.move_forward(map)
        elif action_type == 'move-backward':
            player.move_backward(map)
        else:
            logging.warn(
                'Unrecognized input from player [%s]: %s',
                player_identity,
                action
            )

    def dumps(self):
        return ''


class Action:
    def __init__(self, action_type, delta=None):
        self.action_type = action_type
        self.delta = delta
