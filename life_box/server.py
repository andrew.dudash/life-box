import socket
import logging
import threading
import json
import signal
import time

import message
import game
import network


class GameServer:
    def __init__(self, server_address, server_port=network.DEFAULT_SERVER_PORT, tick_frequency=game.DEFAULT_TICK_FREQUENCY, inactive_player_timeout_threshold=network.INACTIVE_TIMEOUT_THRESHOLD):
        self.tick_frequency = tick_frequency
        self.tick_period = 1 / self.tick_frequency
        self.inactive_player_timeout_threshold = inactive_player_timeout_threshold * 1e9
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server_socket.bind((server_address, server_port))
        self.world_update_thread = threading.Thread(
            target=self.world_update_thread_callback
        )
        self.message_handle_thread = threading.Thread(
            target=self.message_handle_thread_callback
        )
        self.world_publish_thread = threading.Thread(
            target=self.world_publish_thread_callback
        )
        self.world_lock = threading.Lock()
        self.world = game.World()
        self.map_lock = threading.Lock()
        self.map = game.NULL_MAP
        self.connection_table_lock = threading.Lock()
        self.connection_table = {}
        self.connection_timeout_table_lock = threading.Lock()
        self.connection_timeout_table = {}
        self.kill_event = threading.Event()

    def start(self):
        assert not self.kill_event.is_set()
        logging.info('Starting server.') #TODO! add port and IP
        self.world_update_thread.start()
        self.message_handle_thread.start()
        self.world_publish_thread.start()


    def world_update_thread_callback(self):
        while not self.kill_event.is_set():
            start_time = time.time()
            with self.world_lock:
                self.world.update()
            with self.connection_table_lock, self.connection_timeout_table_lock:
                self.purge_inactive_players()
            end_time = time.time()
            elapsed_time = end_time - start_time
            if elapsed_time > self.tick_period:
                logging.warning(
                    'Server too slow to run tick: %s second tick took %s',
                    self.tick_period,
                    elapsed_time
                )
            else:                
                self.kill_event.wait(self.tick_period - elapsed_time) #TODO!

    def world_publish_thread_callback(self):
        while not self.kill_event.is_set():
            with self.connection_table_lock, self.world_lock:
                for client_address in self.connection_table.keys():
                    self.server_socket.sendto(
                        message.make_world_status_message(self.world),
                        client_address
                    )
                    logging.info('Published world state to client: {}'.format(client_address))
            self.kill_event.wait(0.1) #TODO!


    def message_handle_thread_callback(self):
        while not self.kill_event.is_set():
            data, client_address = self.server_socket.recvfrom(network.MAX_MESSAGE_LENGTH)
            try:
                message = json.loads(str(data, 'ascii'))
            except json.decoder.JSONDecodeError as e:
                logging.warning('Could not parse message: {}'.format(e))
            self.server_handle_message(client_address, message)


    def stop(self):
        self.kill_event.set()
        self.message_handle_thread.join()
        self.world_update_thread.join()
        self.world_publish_thread.join()
        logging.info('Server Shutdown')


    def server_handle_message(self, client_address, a_message):
        message_type = a_message['message-type']
        if message_type == 'registration-request':
            with self.connection_table_lock, self.connection_timeout_table_lock, self.world_lock, self.map_lock:
                if client_address in self.connection_table.keys():
                    logging.warning('Host {} tried to register but is already registred.'.format(client_address))
                    player_identity = self.connection_table[client_address]
                else:
                    player_identity = self.world.generate_player()
                    self.connection_table[client_address] = player_identity
                    #self.world.get_player(player_identity)
                    logging.info('Registering player: {}'.format(player_identity))
                self.reset_player_timeout(player_identity)
            self.server_socket.sendto(
                message.make_registration_response_message(
                    identity=player_identity
                ),
                client_address
            )
            self.server_socket.sendto(
                message.make_map_message(self.map),
                client_address
            )
        elif message_type == 'client-actions':
            with self.connection_table_lock, self.connection_timeout_table_lock, self.world_lock, self.map_lock:
                try:
                    player_identity = self.connection_table[client_address]
                    self.reset_player_timeout(player_identity)
                    logging.info('Performing actions for player: {}'.format(player_identity))
                    actions = [
                        game.Action(
                            action_type=action['action-type'],
                            delta=action['delta']
                        )
                        for action in a_message['actions']
                    ]
                    for action in actions:
                        self.world.perform_action(
                            player_identity,
                            action,
                            self.map
                        )
                except Exception as e:
                    logging.warning('Failed to process client-actions: {}'.format(e))
        else:
            logging.warning('Unknown message type: {}'.format(message_type))

    def set_map(self, new_map):
        with self.connection_table_lock, self.map_lock:
            self.map = new_map
            for client_address in self.connection_table.keys():
                self.server_socket.sendto(
                    message.make_map_message(self.map),
                    client_address
                )
                logging.info('Published map to client: {}'.format(client_address))

    def reset_player_timeout(self, player_identity):
        self.connection_timeout_table[player_identity] = time.monotonic_ns()

    def purge_inactive_players(self):
        for client_address in list(self.connection_table.keys()):
            player_identity = self.connection_table[client_address]
            time_elapsed = time.monotonic_ns() - self.connection_timeout_table[player_identity]
            if time_elapsed > self.inactive_player_timeout_threshold:
                del self.connection_table[client_address]
                del self.connection_timeout_table[player_identity]
                self.world.remove_player(player_identity)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    SERVER = GameServer('localhost', inactive_player_timeout_threshold=10)
    TILES = ([1] * 20) + ([1] + ([0] * 18) + [1]) * 18 + ([1] * 20)
    TILES[60 + 10] = 1
    TILES[60 + 11] = 1
    SERVER.set_map(game.Map(
        width=20,
        height=20,
        palette=[
            game.TileType('empty'),
            game.TileType('opaque')
        ],
        tiles=TILES
    ))
    try:
        SERVER.start()
        while True:
            time.sleep(10)
    except KeyboardInterrupt as e:
        pass
    finally:
        SERVER.stop()
