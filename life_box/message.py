import json

def make_target_position_message(identity, position):
    return bytes(json.dumps({
        'message-type': 'target-position',
        'position': {
            'x': position.x,
            'y': position.y
        }
    }), 'ascii')

def make_registration_request_message():
    return bytes(json.dumps({
        'message-type': 'registration-request',
    }), 'ascii')

def make_registration_response_message(identity):
    return bytes(json.dumps({
        'message-type': 'registration-response',
        'identity': identity
    }), 'ascii')


def make_world_status_message(world_status):
    return bytes(json.dumps({
        'message-type': 'world-status',
        'players': [{
            'identity': player.identity,
            'position': {
                'x': player.get_position().x,
                'y': player.get_position().y,
            },
            'direction': player.direction
        }
        for player in world_status.players
        ]
    }), 'ascii')


def make_map_message(map):
    return bytes(json.dumps({
        'message-type': 'map',
        'palette': [
            {'type': palette.type}
            for palette in map.palette
        ],
        'grid': {
            'width': map.width,
            'height': map.height,
            'tiles': map.tiles
        }
    }), 'ascii')


def make_client_actions_message(identity, actions):
    return bytes(json.dumps({
        'message-type': 'client-actions',
        'identity': identity,
        'actions': [
            {'action-type': action.action_type, 'delta': action.delta}
            for action in actions
        ]
    }), 'ascii')


def make_action_message(action):
    return
