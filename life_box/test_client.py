import socket
import logging
import json

import network
import game
import message

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    CLIENT_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    TARGET = ('localhost', network.DEFAULT_SERVER_PORT)
    logging.info('Registering')
    CLIENT_SOCKET.sendto(
        message.make_registration_request_message(),
        TARGET
    )
    a_message, a_server = CLIENT_SOCKET.recvfrom(network.MAX_MESSAGE_LENGTH)
    identity = json.loads(str(a_message, 'ascii'))['identity']
    logging.info('Registration success: {}'.format(identity))
    while True:
        CLIENT_SOCKET.sendto(
            message.make_target_position_message(
                identity=identity,
                position=game.Position(
                    x=10,
                    y=10
                )
            ),
            TARGET
        )
    
