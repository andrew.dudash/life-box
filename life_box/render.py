import math

import pygame

import game

DEFAULT_DIRECTION_RAY_LENGTH = 128
MAX_DEPTH_OF_FIELD = 8

def generate_rays(position, direction, angle_size, ray_count, ray_length):
    angle_increment = angle_size / ray_count
    return [
        (
            position,
            game.Position(
                position.x + (ray_length * math.cos(direction - (angle_size / 2) + (ray * angle_increment))),
                position.y + (ray_length * math.sin(direction - (angle_size / 2) + (ray * angle_increment)))
            )
        )
        for ray in range(ray_count)
    ]

def draw_ray(screen, ray):
    pygame.draw.line(
        screen,
        (0, 64, 0),
        (
            ray[0].x,
            ray[0].y
        ),
        (
            ray[1].x,
            ray[1].y
        )
    )

            

def player_ray(screen, player, offset=0, ray_length=DEFAULT_DIRECTION_RAY_LENGTH):
    player_position = player.get_position()
    pygame.draw.line(
        screen,
        (0, 64, 0),
        (
            player_position.x,
            player_position.y
        ),
        (
            player_position.x + (ray_length * math.cos(player.direction + offset)),
            player_position.y + (ray_length * math.sin(player.direction + offset))
        )
    )

def player_rays(screen, player, angle, ray_count, ray_length=DEFAULT_DIRECTION_RAY_LENGTH):
    right_angle = -1 * angle / 2
    increment = angle / ray_count
    for i in range(ray_count):
        player_ray(
            screen=screen,
            player=player,
            offset=right_angle + (increment * i),
            ray_length=ray_length
        )
    

def render_ray(ray):
    if ray > math.pi:
        ray_y = player.y % tile_width
        ray_x = (player.get_position().y - ray_y) * 1
    if ray < math.pi:
        pass
    if ray == 0 or ray == math.pi:
        ray_x = player.get_position().x
        ray_y = player.get_position().y
    
def render_view(screen, player, field_of_view):
    pass

class Projector:
    def __init__(self, field_of_view, ray_count):
        self.field_of_view = field_of_view
        self.ray_count = ray_count

def make_projector(field_of_view, ray_count, depth_of_field, correct_fish_eye):
    distances = [None] * ray_count
    ray_positions = [None] * ray_count
    ray_angles = [None] * ray_count
    def cast_rays(position, direction, map):
        rays = generate_rays(
            position=position,
            direction=direction,
            angle_size=field_of_view,
            ray_count=ray_count,
            ray_length=1
        )
        for ray_index, ray in enumerate(rays):
            ray_direction = math.atan2(ray[0].y - ray[1].y, ray[0].x - ray[1].x) + math.pi
            # Calculate Horizontal Ray
            horizontal_ray_x = position.x
            horizontal_ray_y = position.y
            if 0 <= ray_direction < math.pi:
                horizontal_ray_y = (position.y // game.TILE_HEIGHT) * game.TILE_HEIGHT
                horizontal_ray_x = ((position.y - horizontal_ray_y) / math.tan(ray_direction + 0.01)) + position.x
            elif math.pi <= ray_direction <= (math.pi * 2):
                horizontal_ray_y = ((position.y // game.TILE_HEIGHT) * game.TILE_HEIGHT) + game.TILE_HEIGHT
                horizontal_ray_x = -1 * (horizontal_ray_y - position.y) / math.tan(ray_direction) + position.x
            else:
                print('bad horizontal')
            for _ in range(depth_of_field):
                tile_position_x = int(horizontal_ray_x // game.TILE_WIDTH)
                tile_position_y = int(horizontal_ray_y // game.TILE_HEIGHT)
                if 0 < ray_direction < math.pi:
                    if map.is_occupied(game.Position(tile_position_x, tile_position_y - 1)):
                        break
                    y_offset = -1 * game.TILE_HEIGHT
                    x_offset = -1 * y_offset / math.tan(ray_direction)
                    horizontal_ray_x += x_offset
                    horizontal_ray_y += y_offset
                elif math.pi < ray_direction < (math.pi * 2):
                    if map.is_occupied(game.Position(tile_position_x, tile_position_y)):
                        break
                    y_offset = game.TILE_HEIGHT
                    x_offset = -1 * y_offset / math.tan(ray_direction)
                    horizontal_ray_x += x_offset
                    horizontal_ray_y += y_offset
            # calculate vertical ray
            vertical_ray_x = position.x
            vertical_ray_y = position.y
            if (math.pi / 2) <= ray_direction <= (math.pi / 2 * 3):
                vertical_ray_x = (position.x // game.TILE_WIDTH) * game.TILE_WIDTH
                vertical_ray_y = 1 * (position.x - vertical_ray_x) * math.tan(ray_direction + 0.01) + position.y
            elif ((math.pi / 2 * 3) < ray_direction <= (math.pi * 2)) or (0 <= ray_direction < (math.pi / 2)):
                vertical_ray_x = (position.x // game.TILE_WIDTH) * game.TILE_WIDTH + game.TILE_WIDTH
                vertical_ray_y = (1 * (position.x - vertical_ray_x) * math.tan(ray_direction + 0.01)) + position.y
            else:
                print('bad vertical')
            for _ in range(depth_of_field):
                tile_position_x = int(vertical_ray_x // game.TILE_WIDTH)
                tile_position_y = int(vertical_ray_y // game.TILE_HEIGHT)
                if (math.pi / 2) < ray_direction < (math.pi / 2 * 3):
                    if map.is_occupied(game.Position(tile_position_x - 1, tile_position_y)):
                        break
                    x_offset = -game.TILE_WIDTH
                    y_offset = -x_offset * math.tan(ray_direction)
                    vertical_ray_x += x_offset
                    vertical_ray_y += y_offset
                elif ((math.pi / 2 * 3) < ray_direction < (math.pi * 2)) or ((0 <= ray_direction < (math.pi / 2))):
                    if map.is_occupied(game.Position(tile_position_x, tile_position_y)):
                        break
                    x_offset = game.TILE_WIDTH
                    y_offset = -x_offset * math.tan(ray_direction)
                    vertical_ray_x += x_offset
                    vertical_ray_y += y_offset
            # pick shortest ray
            squared_vertical_ray_distance = (vertical_ray_x - position.x)**2 + (vertical_ray_y - position.y)**2
            squared_horizontal_ray_distance = (horizontal_ray_x - position.x)**2 + (horizontal_ray_y - position.y)**2
            if squared_vertical_ray_distance > squared_horizontal_ray_distance:
                ray_x = horizontal_ray_x
                ray_y = horizontal_ray_y
                distance = squared_horizontal_ray_distance
            else:
                ray_x = vertical_ray_x
                ray_y = vertical_ray_y
                distance = squared_vertical_ray_distance
            distance = math.sqrt(distance)
            if correct_fish_eye:
                delta_angle = ray_direction - direction
                distance *= math.cos(delta_angle)
            ray_positions[ray_index] = (ray_x, ray_y)
            distances[ray_index] = distance
        return distances, ray_positions
    return cast_rays



def make_top_down_renderer(player_image, depth_of_field, field_of_view, ray_count, tile_mapping, correct_fish_eye):
    cast_rays = make_projector(
        field_of_view=field_of_view,
        ray_count=ray_count,
        depth_of_field=depth_of_field,
        correct_fish_eye=correct_fish_eye
    )
    def render_top_down(world, map, surface):
        # Draw Map Tiles
        for position, tile in map:
            try:
                tile_image = tile_mapping[tile.type]
                surface.blit(
                    tile_image,
                    (
                        game.TILE_WIDTH * position.x,
                        game.TILE_WIDTH * position.y
                    )
                )
            except KeyError:
                logging.error('Unknown tile type: %s', TILE_TYPE)

        # Draw Players and Rays
        for player in world.players:
            player_position = player.get_position()
            surface.blit(
                player_image,
                (
                    player_position.x - (player_image.get_size()[0] / 2),
                    player_position.y - (player_image.get_size()[1] / 2)
                )
            )
            distances, ray_positions = cast_rays(player_position, player.direction, map)
            for ray_x, ray_y in ray_positions:
                draw_ray(
                    surface,
                    (
                        player_position,
                        game.Position(ray_x, ray_y)
                    )
                )
    return render_top_down

def make_first_person_renderer(depth_of_field, field_of_view, ray_count, tile_mapping, correct_fish_eye):
    cast_rays = make_projector(
        field_of_view=field_of_view,
        ray_count=ray_count,
        depth_of_field=depth_of_field,
        correct_fish_eye=correct_fish_eye
    )
    def render_first_person(player, world, map, surface):
        player_position = player.get_position()
        distances, ray_positions = cast_rays(player_position, player.direction, map)
        column_width = surface.get_width() // ray_count
        for column, distance in enumerate(reversed(distances)):
            if distance == 0:
                continue
            column_height = min((32 * surface.get_height()) // distance, surface.get_height())
            column_offset = (surface.get_height() // 2) - (column_height // 2)
            pygame.draw.line(
                surface,
                (0, 64, 0),
                (
                    (column * column_width) + (column_width // 2),
                    column_offset
                ),
                (
                    (column * column_width) + (column_width // 2),
                    column_height + column_offset
                ),
                column_width
            )
    return render_first_person
